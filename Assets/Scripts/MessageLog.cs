﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
  
  
public class MessageLog : MonoBehaviour
{
	
	public int maxLines = 8;
	private Queue<string> queue = new Queue<string>();
	private string Log = "";
	private Text mytext;
	public Scrollbar vertbar;

	void Start()
	{
		mytext = GetComponent<Text>();
		NewMessage("");
		vertbar.value = 0;
	}

 	public void NewMessage(string message)
 	{

		if (queue.Count >= maxLines) queue.Dequeue();
		
		queue.Enqueue(message);
		
		Log = "";
		foreach (string st in queue) Log = Log + st + "\n";
		vertbar.value = 0;
	}
  
  
	void OnGUI()
	{	
		mytext.text = Log;

	}


}