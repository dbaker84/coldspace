﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour {


    private Rigidbody rb;
    public int NumRandomStations;
    public GameObject RandomStationPrefab;

    public GameObject ParentBody;

    public float StartDistance;
    public float CurrentDistance;

    

    public string PlanetType;
 
    void Start () {
        rb = GetComponent<Rigidbody>();

        // start a spawn loop for stations
        if(NumRandomStations > 0 && RandomStationPrefab)
        {
            for(int i = 0; i <= NumRandomStations; i++)
            {
                Vector3 spawnPoint;
                float radian = Random.Range(0f, Mathf.PI*2);
                float x = Mathf.Cos(radian);
                float z = Mathf.Sin(radian);
                spawnPoint = transform.position + (new Vector3 (x,0,z) * (transform.localScale.x/2.04f));
                GameObject NewStation = Instantiate(RandomStationPrefab, spawnPoint, transform.rotation);
                NewStation.transform.SetParent(transform);
                NewStation.transform.localScale = new Vector3(1,1,1) / transform.localScale.x;
                NewStation.transform.LookAt(transform.position);
                NewStation.transform.Rotate(-90,0,0);
            }
        }

        GetComponent<Renderer>().material.color = new Color( Random.value, Random.value, Random.value, 1.0f );

    }

    void Update()
    {
        CurrentDistance = (transform.position - ParentBody.transform.position).sqrMagnitude;
    }



    
		
}
