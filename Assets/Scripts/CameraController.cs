﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	public float MainCameraDistance;
	public GameObject Player;

	void Update () {
		transform.rotation = Quaternion.Euler(90,0,0);
		transform.position = Player.transform.position + new Vector3(0,MainCameraDistance,0);
	}
}
