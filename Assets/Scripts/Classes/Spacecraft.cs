﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spacecraft : MonoBehaviour{

	public Container CargoHold;
	public string Name;

	public int SizeClass;
	
	public GameManager gameManager;

	public float MaxVelocity;
	public float MaxThrust;
	public float RotationSpeed;
	public float Thrust;
	public float ThrustModifier; //0..1


	public float HullIntegrity;
	public float ArmorIntegrity;

	public float HullRating; //max hull
	public float ArmorRating; //max armor

	public GameObject DestructionPE;
	public bool Exploding = false;
	public bool Exploded = false;
	public Vector3 ExplosionRandomRange;
	public AudioClip DestructionPop;
	public AudioClip DestructionBoom;

	public bool Landed;
	public PortController LandedPort;
	public GameObject LandedPortGO;
	public AudioClip LandingSound;

	AudioSource DestructionAudio;
	
	public Weapon[] Weapons;
	public Weapon ActiveWeapon;

	public float Money;
	public bool WeaponsHot;

	void LateUpdate()
	{
		if(HullIntegrity <= 0 && !Exploding && !Exploded)
		{
			//Destroy(gameObject,3);
			Exploding = true;
			//InvokeRepeating("HullExplosion", 0, 0.5f);
			gameObject.AddComponent<TriangleExplosion>();
			if(DestructionPop || DestructionBoom) DestructionAudio = gameObject.AddComponent<AudioSource>();
			Invoke("Shatter", 3);
		}
		if(Exploding && Time.frameCount % 30 == 0)
		{
			HullExplosion();
		}

		transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);

	}

	void HullExplosion()
	{
		GameObject GO = Instantiate(DestructionPE, transform.position + new Vector3(Random.Range(-ExplosionRandomRange.x,ExplosionRandomRange.x),
			Random.Range(-ExplosionRandomRange.y,ExplosionRandomRange.y), Random.Range(-ExplosionRandomRange.z,ExplosionRandomRange.z)), transform.rotation);
		Destroy(GO, GO.GetComponent<ParticleSystem>().main.duration);
		if(DestructionPop) DestructionAudio.PlayOneShot(DestructionPop);
	}

	void Shatter()
	{
 			StartCoroutine(gameObject.GetComponent<TriangleExplosion>().SplitMesh(true));
			Exploding = false;
			Exploded = true;
			if(DestructionBoom) DestructionAudio.PlayOneShot(DestructionBoom);
			gameManager.mlog.NewMessage(Name + " has been destroyed.");
	}

	public void ApplyDamage(float Damage, float ArmorPiercingRating)
	{
		float DamageToArmor;
		float DamageToHull;
		if(ArmorIntegrity > 0) 
		{
			DamageToHull = Damage * ArmorPiercingRating;
			DamageToArmor = Damage - DamageToHull;
		}
		else
		{
			DamageToArmor = 0;
			DamageToHull = Damage;
		}

		HullIntegrity -= DamageToHull;
		ArmorIntegrity -= DamageToArmor;

	}

}
