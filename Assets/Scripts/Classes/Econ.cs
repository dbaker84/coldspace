﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Econ {

	public static readonly float MarketArbitrage = 0.02f;
	public static readonly float MarketFactor = 0.2f;

	public static readonly List<string> ItemList = new List<string>
	{
		{ "Electronics" },
		{ "Basic Consumer Goods" },
		{ "Construction Materials" },
		{ "Luxury Consumer Goods" },
		{ "Military Equipment" },
		{ "Basic Foodstuffs" },
		{ "Luxury Foodstuffs" },
		{ "Iron" },
		{ "Titanium" },
		{ "Plutonium" },
		{ "Precious Metals" },
		{ "Silicates" },
		{ "Noble Gases" },
		{ "Common Gases" },
		{ "Petrochemicals" },
		{ "Water" },
		{ "High Tech Robots" },
		{ "Medical Supplies" },
		{ "Robots" },
		{ "Biomass" },
		{ "Livestock" },
		{ "Common Plants" },
		{ "Rare Plants" },
		{ "Lumber" },
		{ "Psychoactives" },
		{ "Stimulants" },
		{ "Alcohol" },
		{ "Toxins" },
		{ "Slaves" }
	};	

	public static readonly Dictionary<string, float> MarketPrices = new Dictionary<string, float>
	{
		{ "Electronics", 200.0f },
		{ "Basic Consumer Goods", 100.0f },
		{ "Construction Materials", 100.0f },
		{ "Luxury Consumer Goods", 300.0f },
		{ "Military Equipment", 400.0f },
		{ "Basic Foodstuffs", 100.0f },
		{ "Luxury Foodstuffs", 300.0f },
		{ "Iron", 100.0f },
		{ "Titanium", 150.0f },
		{ "Plutonium", 300.0f },
		{ "Precious Metals", 300.0f },
		{ "Silicates", 150.0f },
		{ "Noble Gases", 200.0f },
		{ "Common Gases", 100.0f },
		{ "Petrochemicals", 150.0f },
		{ "Water", 100.0f },
		{ "High Tech Robots", 500.0f },
		{ "Medical Supplies", 300.0f },
		{ "Robots", 400.0f },
		{ "Biomass", 50.0f },
		{ "Livestock", 150.0f },
		{ "Common Plants", 100.0f },
		{ "Rare Plants", 350.0f },
		{ "Lumber", 100.0f },
		{ "Psychoactives", 1200.0f },
		{ "Stimulants", 800.0f },
		{ "Alcohol", 250.0f },
		{ "Toxins", 600.0f },
		{ "Slaves", 1500.0f }
	};

	public static readonly Dictionary<string, List<string>> Producers = new Dictionary<string, List<string>>
	{
		{"Electronics", new List<string> {"Manufacturing"}},
		{"Basic Consumer Goods", new List<string> {"Manufacturing", "DT3", "DT4"}},
		{"Construction Materials", new List<string> {"Manufacturing"}},
		{"Luxury Consumer Goods", new List<string> {"Manufacturing", "DT1", "DT2"}},
		{"Military Equipment", new List<string> {"Manufacturing", "Technology", "Multigov", "Federation"}},
		{"Basic Foodstuffs", new List<string> {"Manufacturing", "DT4", "DT5"}},
		{"Luxury Foodstuffs", new List<string> {"Manufacturing"}},
		{"Iron", new List<string> {"Telluric", "Lithic", "Barren", "Extraction"}},
		{"Titanium", new List<string> {"Lithic", "Chthonic", "Telluric", "Extraction"}},
		{"Plutonium", new List<string> {"Chthonic", "Barren", "Extraction"}},
		{"Precious Metals", new List<string> {"Frozen", "Chthonic", "Lithic", "Extraction", "DT5", "Socialist"}},
		{"Silicates", new List<string> {"Lithic", "Barren", "Telluric", "Extraction"}},
		{"Noble Gases", new List<string> {"Frozen", "Aeric", "Chthonic", "Extraction"}},
		{"Common Gases", new List<string> {"Frozen", "Aeric", "Telluric", "Oceanic", "Extraction", "Growing"}},
		{"Petrochemicals", new List<string> {"Oceanic", "Telluric", "Vivic", "Extraction", "Growing"}},
		{"Water", new List<string> {"Oceanic", "Telluric", "Frozen", "Extraction"}},
		{"High Tech Robots", new List<string> {"Technology"}},
		{"Medical Supplies", new List<string> {"Technology", "DT2", "DT3"}},
		{"Robots", new List<string> {"Technology"}},
		{"Biomass", new List<string> {"Telluric", "Oceanic", "Vivic", "Conservation"}},
		{"Livestock", new List<string> {"Telluric", "Conservation", "DT5"}},
		{"Common Plants", new List<string> {"Telluric", "Oceanic", "Vivic", "Conservation", "Growing", "DT5"}},
		{"Rare Plants", new List<string> {"Telluric", "Oceanic", "Vivic", "Conservation", "Growing"}},
		{"Lumber", new List<string> {"Telluric", "Vivic", "Conservation", "Growing", "DT5"}},
		{"Psychoactives", new List<string> {"Black", "Market", "Technology", "Growing", "Conservation", "Anarchy"}},
		{"Stimulants", new List<string> {"Black", "Market", "Technology", "Anarchy"}},
		{"Alcohol", new List<string> {"Black", "Market", "Anarchy"}},
		{"Toxins", new List<string> {"Chthonic", "Black", "Market", "Technology", "Anarchy", "Autocracy"}},
		{"Slaves", new List<string> {"Black", "Market", "DT5", "Anarchy", "Autocracy"}}
	};

	public static readonly Dictionary<string, List<string>> Consumers = new Dictionary<string, List<string>>
	{
		{"Electronics", new List<string> {"Technology", "DT1", "DT2", "DT3"}},
		{"Basic Consumer Goods", new List<string> {"Recreation", "DT4", "DT5", "Anarchy", "Socialist"}},
		{"Construction Materials", new List<string> {"Vivic", "Growing", "Extraction", "DT4", "DT5", "Multigov"}},
		{"Luxury Consumer Goods", new List<string> {"Recreation", "DT1", "DT2", "Theocracy", "Federation", "Monarchy", "Autocracy"}},
		{"Military Equipment", new List<string> {"Autocracy", "Anarchy"}},
		{"Basic Foodstuffs", new List<string> {"Frozen", "Aeric", "Chthonic", "Barren", "Lithic", "DT4", "DT5", "Anarchy", "Socialist"}},
		{"Luxury Foodstuffs", new List<string> {"Recreation", "DT1", "DT2", "Theocracy", "Federation", "Monarchy", "Autocracy"}},
		{"Iron", new List<string> {"Manufacturing"}},
		{"Titanium", new List<string> {"Manufacturing", "Technology", "DT1", "DT2"}},
		{"Plutonium", new List<string> {"Technology", "DT1", "Anarchy"}},
		{"Precious Metals", new List<string> {"Recreation", "Technology", "DT1", "DT2", "Federation", "Monarchy", "Multigov", "Autocracy"}},
		{"Silicates", new List<string> {"Manufacturing", "Technology"}},
		{"Noble Gases", new List<string> {"Technology", "Recreation"}},
		{"Common Gases", new List<string> {"Chthonic", "Growing", "Manufacturing"}},
		{"Petrochemicals", new List<string> {"Manufacturing", "Recreation", "DT4", "DT5"}},
		{"Water", new List<string> {"Chthonic", "Lithic", "Barren", "Aeric", "Conservation", "DT4", "DT5"}},
		{"High Tech Robots", new List<string> {"Recreation", "Manufacturing", "DT1", "DT2", "Federation", "Autocracy"}},
		{"Medical Supplies", new List<string> {"Recreation"}},
		{"Robots", new List<string> {"Growing", "Extraction", "DT3", "DT4", "Autocracy", "Socialist"}},
		{"Biomass", new List<string> {"Growing", "Manufacturing", "DT5"}},
		{"Livestock", new List<string> {"Barren", "Manufacturing", "Recreation"}},
		{"Common Plants", new List<string> {"Barren", "Lithic", "Manufacturing"}},
		{"Rare Plants", new List<string> {"Technology", "Recreation", "Black", "Market", "Monarchy"}},
		{"Lumber", new List<string> {"Barren", "Lithic", "Manufacturing"}},
		{"Psychoactives", new List<string> {"Recreation", "DT1", "DT2", "Federation", "Multigov"}},
		{"Stimulants", new List<string> {"Recreation", "Manufacturing", "Multigov"}},
		{"Alcohol", new List<string> {"Extraction", "Growing", "Recreation", "DT3", "DT4", "DT5", "Monarchy"}},
		{"Toxins", new List<string> {"Vivic", "Technology", "DT5"}},
		{"Slaves", new List<string> {"Growing", "Extraction", "Recreation", "Manufacturing", "DT1", "DT5", "Monarchy", "Multigov"}}
	};

}
