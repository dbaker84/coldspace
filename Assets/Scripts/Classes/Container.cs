﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Container {

	public int Capacity;
	public int CapUsed;
	public List<InvObj> Contents;

	public float CapAvail {get { return Capacity - CapUsed;}}

     public Container(int capacity)
     {
         Capacity = capacity;
		 Contents = new List<InvObj>();
		 UpdateCapUsed();
     }

	public void UpdateCapUsed()
	{
		CapUsed = 0;
		foreach(InvObj i in Contents)
		{
			CapUsed += i.Units;
		}
	}

	public void AddItem(InvObj item)
	{
		Contents.Add(item);
		UpdateContainer();
	}

	public void UpdateContainer()
	{
		Contents.RemoveAll(r => r.Units == 0);
		UpdateCapUsed();
	}



}
