﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class InvObj {

	public string Name;
	public int Units;
	public float BasePrice;

     public InvObj(string name, int units)
     {
		Name = name;
		Units = units;
     }

}
