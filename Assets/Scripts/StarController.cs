﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarController : MonoBehaviour {

    string[] PlanetTypes = new string[] {"Frozen", "Aeric", "Oceanic", "Telluric", "Chthonic", "Barren", "Lithic", "Vivic"};
	string[] IndustryTypes = new string[] {"Manufacturing", "Extraction", "Technology", "Conservation", "Recreation", "Biogenics"};
	string[] GovernmentTypes = new string[] {"Tribal", "Socialist", "Autocratic", "Monarchic", "Factional", "Federal", "Theocractic"};

	string[] Descriptors = new string[] {"no", "very low", "low", "moderate", "high", "very high"};
	string[] Adjectives = new string[] {"no", "very low", "low", "moderate", "high", "very high"};

	public int NumberofOrbitingBodies;
	public int SpacingRatio;
	public GameObject BodyPrefab;
	public float Size;

	public List<PortController> PortList = new List<PortController>();

	public TextAsset Cities;
	static public List<string> PortNameList = new List<string>();


	// Use this for initialization
	void Start () {

		string TempString = Cities.text;
		List <string> list = new List<string>();
        //list.AddRange(TempString.Split("\n"[0]));
		string[] lines = Cities.text.Split ("\n" [0]);
		foreach(string line in lines)
		{
			PortNameList.Add(line);
		}

		//lets generate some random planets
		for(int i = 0; i != NumberofOrbitingBodies; i++)
		{
			GenerateOrbitingBody(new Vector3(( Random.Range(-NumberofOrbitingBodies * SpacingRatio,NumberofOrbitingBodies * SpacingRatio)),0,(Random.Range(-NumberofOrbitingBodies * SpacingRatio,NumberofOrbitingBodies * SpacingRatio))));
		}
	}

	void GenerateOrbitingBody(Vector3 position)
	{
		GameObject NewBody = Instantiate(BodyPrefab, position, transform.rotation);
		PortController NewPort = NewBody.transform.Find("Port").GetComponent<PortController>();

		PortList.Add(NewPort);

		float Scale = Random.Range(30, 50);
		NewBody.transform.localScale = new Vector3(Scale, Scale, Scale);

		NewBody.name = NewBody.GetInstanceID().ToString();
		NewBody.GetComponent<PlanetController>().PlanetType = PlanetTypes[Random.Range(0,PlanetTypes.Length)];

		NewPort.PortName = PortNameList[Random.Range(0, PortNameList.Count)];
		NewPort.Industry = IndustryTypes[Random.Range(0,IndustryTypes.Length)];
		NewPort.Government = GovernmentTypes[Random.Range(0,GovernmentTypes.Length)];
		NewPort.DevelopmentTier = Random.Range(1,6);
		NewPort.DTS = "DT" + NewPort.DevelopmentTier.ToString();
		NewPort.Stability = Random.Range(1,6);

		NewBody.GetComponent<PlanetController>().ParentBody = gameObject;
		NewBody.GetComponent<PlanetController>().GetComponent<Orbiter>().StartOrbit();

	}
}
