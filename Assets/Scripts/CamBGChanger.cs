﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamBGChanger : MonoBehaviour {

	bool toggle = true;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Alpha0))
		{
			toggle = !toggle;
			if(toggle) GetComponent<Camera>().backgroundColor = Color.black;
			else GetComponent<Camera>().backgroundColor = Color.gray;
		}
	}
}
