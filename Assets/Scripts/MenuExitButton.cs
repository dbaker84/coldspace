﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuExitButton : MonoBehaviour {

	public GameObject MenuToClose;

	public void CloseMenu()
	{
		MenuToClose.SetActive(false);
		GameObject.Find("Player").GetComponent<PlayerController>().InMenu = false;
	}

}
