﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortController : MonoBehaviour {

	public string PortName;
	public string Industry;
	public string Government;
	public int DevelopmentTier;
	public string PlanetType;
	public string DTS; //developmenttierstring
	public int Stability; //stability, 0..1

	public GameManager gameManager;

	public Container PortStorage;
	public float Money;
	public Dictionary<string, float> PriceIndex;

	public float SellFactor;
	public float BuyFactor;

	string[] Descriptors = new string[] {"no", "very low", "low", "moderate", "high", "very high"};
	string[] Adjectives = new string[] {"no", "nominally", "minimally", "moderately", "highly", "very highly"};
	

	void Start () {
		gameManager = gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		PortStorage = new Container(int.MaxValue);
		PlanetType = transform.parent.GetComponent<PlanetController>().PlanetType;


		foreach(string Item in Econ.ItemList)
		{
			PortStorage.AddItem(new InvObj(Item, 100));
		}

		Money = Random.Range(0.8f,4.0f) * 1000000;

		BuildPriceIndex();

	}

	public string GetPortDesc () {
		string Desc = "";
		Desc = PlanetType + " planet - ";
		Desc = Desc + Adjectives[DevelopmentTier] + " developed " + Industry.ToLower() + " industry controlled by a " +
			Adjectives[Stability] + " stability " + Government.ToLower() + " government."; 

		return Desc;
		
	}

	public void BuildPriceIndex()
	{
		PriceIndex = new Dictionary<string, float>  {};
		foreach(string Item in Econ.ItemList)
		{
			PriceIndex.Add(Item, 0);

			if(Econ.Producers[Item].Contains(PlanetType)) PriceIndex[Item] -= 1;
			if(Econ.Producers[Item].Contains(Industry)) PriceIndex[Item] -= 1;
			if(Econ.Producers[Item].Contains(Government)) PriceIndex[Item] -= 1;
			if(Econ.Producers[Item].Contains(DTS)) PriceIndex[Item] -= 1;

			if(Econ.Consumers[Item].Contains(PlanetType)) PriceIndex[Item] += 1;
			if(Econ.Consumers[Item].Contains(Industry)) PriceIndex[Item] += 1;
			if(Econ.Consumers[Item].Contains(Government)) PriceIndex[Item] += 1;
			if(Econ.Consumers[Item].Contains(DTS)) PriceIndex[Item] += 1;

			PriceIndex[Item] += Random.Range(-(1.0f - (Stability/5)), (1.0f - (Stability/5)));

			//Debug.Log("Port " + PortName + " has a PriceIndex of " + PriceIndex[Item] + " for " + Item);			
		}
	}

	public float GetPrice(string Item)
	{
		return Econ.MarketPrices[Item] * (1.0f + (PriceIndex[Item] * Econ.MarketFactor));
	}

}
