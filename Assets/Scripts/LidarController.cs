﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LidarController : MonoBehaviour {

	public PlayerController player;

	public float Resolution;
	public float Range;
	public float repeatFireTicker;
	public float RaysPerSecond;
	public Vector3 RayVector;

	public LayerMask RayMask;
	public LineRenderer RayLine;

	public GameObject LidarGhost;

	public Slider ResolutionSlider;


	// Use this for initialization
	void Start () {
		repeatFireTicker = 0;
		//RayVector = Vector3.forward;
	}
	
	// Update is called once per frame
	void Update () {
		if(player.MassCamera.enabled)
		{
			if(repeatFireTicker <= 0)
			{
				FireRay();
				repeatFireTicker = 1;
			}

			if(repeatFireTicker > 0) repeatFireTicker -= RaysPerSecond * Time.deltaTime;
		}

	}

	public void ChangeResolution()
	{
		Resolution = ResolutionSlider.value + 0.1f;
	}

	void FireRay()
	{
		RaycastHit hit;

		if (Physics.Raycast(transform.position, RayVector, out hit, Range, RayMask ))
        {
			Vector3[] RayEnds = new Vector3[ 2 ] { transform.position, hit.point };
        	RayLine.SetPositions( RayEnds );
			//Debug.Log(hit.collider.gameObject.transform.parent.name);
			GameObject Blip = Instantiate(LidarGhost, hit.point, transform.rotation);
			Destroy(Blip, 700.0f/(Resolution * RaysPerSecond));
		}
		else
		{
			RayLine.SetPosition(0, transform.position);
			RayLine.SetPosition(1, (RayVector * Range) + transform.position);
		}

		RayVector = Quaternion.AngleAxis(Resolution, Vector3.up) * RayVector;
	}
}
