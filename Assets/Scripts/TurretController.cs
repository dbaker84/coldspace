﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : MonoBehaviour {

    public GameObject Target;
	public float AnglesPerSecond;

	// Update is called once per frame
	void Update () {
		if(Target)
		{
			Vector3 TargetPoint = Target.transform.position;
			Vector3 Forward = transform.TransformDirection(Vector3.forward).normalized;
			Vector3 ToTarget = (transform.position - TargetPoint).normalized;

			if(Vector3.Dot(-Forward, ToTarget) < 0.99f)
			{
				float TurnRatio = Mathf.Clamp(1.02f - Vector3.Dot(-Forward, ToTarget), 0, 1);
				if (Vector3.Angle(-Forward, ToTarget) > 5) TurnRatio = 1.0f;
				else Mathf.Clamp(Vector3.Angle(-Forward, ToTarget) / 180, 0, 1);

				Vector3 relativePoint = transform.InverseTransformPoint(TargetPoint);
				if (relativePoint.x > 0.0) TurnRatio *= -1;
					//print ("Object is to the left");
				//else if (relativePoint.x < 0.0)
					//print ("Object is to the right");

				transform.RotateAround(transform.position, Vector3.up, AnglesPerSecond * Time.deltaTime * TurnRatio);
			}

			if(Vector3.Dot(Forward, ToTarget) > GetComponent<Weapon>().DotLimit)
			{
				GetComponent<Weapon>().AttemptToFire();
			}
		}


	}
}
