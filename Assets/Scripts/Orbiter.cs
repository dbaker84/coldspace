using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Orbiter : MonoBehaviour {
 
    // orbits around a "star" at the origin with fixed mass
    public float starMass = 1000f;
    public Vector3 BodyRotation;
    public GameObject ParentBody;

    private Rigidbody rb;
    private bool OrbitStarted = false;
 
    // Use this for initialization
    public void StartOrbit () {
        rb = GetComponent<Rigidbody>();
        ParentBody = gameObject.GetComponent<PlanetController>().ParentBody;

        float initV = Mathf.Sqrt(starMass / (transform.position - ParentBody.transform.position).magnitude);

        //GetComponent<Rigidbody>().velocity = new Vector3(initV, 0, 0);

        //Debug.Log(initV);

        Vector3 toPlanet = (transform.position - ParentBody.transform.position);
        Vector3 perpPlanet = new Vector3(toPlanet.z, 0, -toPlanet.x).normalized;

        //Debug.Log(toPlanet + " " + perpPlanet);

        GetComponent<Rigidbody>().velocity = perpPlanet * initV;
        
        rb.AddTorque(BodyRotation);
        OrbitStarted = true;

        gameObject.GetComponent<PlanetController>().StartDistance = (transform.position - ParentBody.transform.position).sqrMagnitude;
    }
 
    // Update is called once per frame
    public void FixedUpdate () {
        if(OrbitStarted)
        {
            float r = Vector3.Magnitude(transform.position - ParentBody.transform.position);
            float totalForce = -(starMass) / (r * r);
            Vector3 force = (transform.position - ParentBody.transform.position).normalized * totalForce;
            rb.AddForce(force);  
        }


    }

}