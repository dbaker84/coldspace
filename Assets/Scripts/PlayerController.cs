﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : Spacecraft {



	public GameObject Reticle;

	Rigidbody rb;

	public GameObject PortMenu;
	public GameObject MarketMenu;
	public GameObject LIDARMenu;
	public bool InMenu;

	public GameObject HelpScreen;

	public AudioSource EngineHumSource;
	public AudioSource ClipPlayer;

	public ParticleSystem EngineEmitter;

	public Camera MainCamera;
	public Camera MassCamera;

	public GameObject MyBlip;

	public float HyperSpeed;
	public float HDCooldownTime;
	public float HDCooldownLeft;
	public bool HyperActive;

	public AudioClip EngageHD;
	public AudioClip DisengageHD;

	public Text CooldownTimer;
	public Text WeaponsIndicator;
	public Text CruiseIndicator;

	public GameObject Indicators;



	


	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		rb = GetComponent<Rigidbody>();

		PortMenu.SetActive(false);
		MarketMenu.SetActive(false);
		LIDARMenu.SetActive(false);
		
		MyBlip.SetActive(true);


		CargoHold = new Container(10);
		rb.ResetCenterOfMass();
		WeaponsToggle(false);
		HyperToggle(false);
	}

	public void WeaponsToggle(bool b = false)
	{
		WeaponsHot = b;
		Reticle.SetActive(b);
		if(WeaponsHot) WeaponsIndicator.color = Color.green;
		else WeaponsIndicator.color = Color.red;
	}

	public void HyperToggle(bool b = false)
	{
		HyperActive = b;
		if(HyperActive) CruiseIndicator.color = Color.green;
		else CruiseIndicator.color = Color.red;
	}


	void Update()
	{

		
		if(HDCooldownLeft >= 0)
		{
			CooldownTimer.text = (HDCooldownLeft - Time.deltaTime).ToString("F1");
			if(HDCooldownLeft > 0) CooldownTimer.color = Color.red;
			else if(HDCooldownLeft == 0) CooldownTimer.color = Color.green;

			HDCooldownLeft -= Time.deltaTime;
			
			if(HDCooldownLeft < 0) HDCooldownLeft = 0;

		}

		if (Input.GetKeyDown(KeyCode.F1))
		{
			HelpScreen.SetActive(!HelpScreen.activeSelf);
		}

		if (Input.GetKeyDown(KeyCode.C))
		{
			MainCamera.enabled = !MainCamera.enabled;
			MassCamera.enabled = !MassCamera.enabled;

			if(MassCamera.enabled)
			{
				LIDARMenu.SetActive(true);
				InMenu = true;
			}
			else
			{
				LIDARMenu.SetActive(false);
				InMenu = false;
			}
		}

		if (Input.GetKeyDown(KeyCode.Z) && MainCamera.enabled && !Landed)
		{
			if(WeaponsHot) WeaponsToggle(false);
			else WeaponsToggle(true);
		}

		if (Input.GetKeyDown(KeyCode.H) && !Landed && HDCooldownLeft <= 0)
		{
			if(HyperActive)
			{
				HyperToggle(false);
				ClipPlayer.PlayOneShot(DisengageHD);
			}
			else
			{
				HyperToggle(true);
				ClipPlayer.PlayOneShot(EngageHD);
			}

			HDCooldownLeft = HDCooldownTime;
		}


		if(ActiveWeapon && WeaponsHot && Input.GetMouseButton(0))
		{
			ActiveWeapon.AttemptToFire();
		}

		if (Input.GetKeyDown(KeyCode.I))
		{
			CargoHold.AddItem(new InvObj("Ice", 4));

			Debug.Log("Capacity: " + CargoHold.Capacity.ToString());
			Debug.Log("Cap. Used: " + CargoHold.CapUsed.ToString());
			Debug.Log("Available: " + CargoHold.CapAvail.ToString());
		}

		if (Input.GetKeyDown(KeyCode.L) && Landed)
		{
			LaunchFromPort();
		}

		if (Input.GetKeyDown(KeyCode.T) && LandedPort)
		{		
			//LandAtPort();
		}

		if (ThrustModifier < 1.0f) ThrustModifier *= (1f + (0.5f * Time.deltaTime));
		if (ThrustModifier > 1.0f) ThrustModifier = 1.0f;

		if(!Landed) EngineHumSource.volume = Mathf.Abs(Input.GetAxis("Vertical"));

        if (Input.GetKey("escape"))  Application.Quit();
        
	}

	void OnTriggerEnter (Collider c)
    {
        if(c.gameObject.tag == "Dock")
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            transform.parent = c.transform.parent;
            rb.isKinematic = true;
            ThrustModifier = 0;
            Landed = true;
			
			if(c.gameObject.GetComponent<PortController>()) LandedPort = c.gameObject.GetComponent<PortController>();
			LandedPortGO = c.gameObject.transform.parent.gameObject;
            EngineHumSource.volume = 0f;
            if(LandingSound) ClipPlayer.PlayOneShot(LandingSound, 1.0f);

			Indicators.SetActive(false);

			WeaponsHot = false;

			gameManager.mlog.NewMessage("Welcome to " + LandedPort.PortName);

			LandAtPort();
        }
    }

	public void LandAtPort()
	{
		PortMenu.SetActive(true);
		PortMenu.GetComponent<PortMenuController>().UpdateMenu();

		gameManager.mlog.NewMessage("Entering Port");
	}

	public void LaunchFromPort()
	{
		float startTime = Time.time;
		Vector3 EjectVector = (transform.position - transform.parent.position).normalized;
		transform.parent = null;
		rb.isKinematic = false;
		ThrustModifier = 0.2f;
		Landed = false;
		LandedPort = null;
		PortMenu.SetActive(false);

		Indicators.SetActive(true);

		//eject from pad
		

		rb.AddForce(EjectVector * 2.5f, ForceMode.Impulse);

		LandedPortGO = null;

		gameManager.mlog.NewMessage("See you, Space Cowboy");
	}

	// Update is called once per frame
	void FixedUpdate () {

		if(HyperActive)
		{
			rb.AddForce(-transform.forward * Mathf.Clamp(Input.GetAxis("Vertical"), 0.5f, 1.0f) * Thrust * ThrustModifier * HyperSpeed);
			rb.AddTorque(Vector3.up * Input.GetAxis("Horizontal") * RotationSpeed * 0.2f);
		}
		else
		{
			rb.AddForce(-transform.forward * Mathf.Clamp(Input.GetAxis("Vertical"), -0.5f, 1.0f) * Thrust * ThrustModifier);
			rb.AddTorque(Vector3.up * Input.GetAxis("Horizontal") * RotationSpeed);
		}
	}
}
