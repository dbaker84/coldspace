﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargoController : MonoBehaviour {

	public Vector3 BodyRotation;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		rb.AddTorque(BodyRotation);
	}
}
