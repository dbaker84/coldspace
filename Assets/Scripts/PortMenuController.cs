﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortMenuController : MonoBehaviour {

	public PlayerController player;
	
	public Button MarketButton;
	public GameObject MarketMenu;



	public void UpdateMenu()
	{
		GameObject.Find("PortNameText").GetComponent<Text>().text = player.LandedPort.PortName.ToString();
	}



    void Start()
    {
        //Button MarketButton = transform.Find("MarketButton").gameObject.GetComponent<Button>();
        MarketButton.onClick.AddListener(MarketMenuOpen);
    }

    void MarketMenuOpen()
    {
        MarketMenu.SetActive(true);
		MarketMenu.GetComponent<MarketMenuController>().UpdateMarketMenu();
        player.InMenu = true;
    }

    void LaunchFromPort()
    {
        player.LaunchFromPort();
    }

}
