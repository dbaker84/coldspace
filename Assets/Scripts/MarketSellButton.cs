﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketSellButton : MonoBehaviour {

	public InvObj EntryItem;
	public PortController Port;
	public PlayerController player;

	public int Quantity;
	public Text InputField;

	public void Start()
	{
		GetComponent<Button>().onClick.AddListener(SellItem);		
	}

	
	public void SellItem()
	{
		Quantity = int.Parse(InputField.text);
		if(Quantity > 0)
		{
			InvObj PlayerItem = player.CargoHold.Contents.Find(r => r.Name == EntryItem.Name);
			InvObj PortItem = Port.PortStorage.Contents.Find(r => r.Name == EntryItem.Name);
			
			if(PlayerItem != null && Quantity <= PlayerItem.Units)
			{
				player.Money += Quantity * (Port.GetPrice(PortItem.Name) * (1.0f - Econ.MarketArbitrage));
				Port.Money -= Quantity * (Port.GetPrice(PortItem.Name) * (1.0f - Econ.MarketArbitrage));

				PlayerItem.Units -= Quantity;
				PortItem.Units += Quantity;	

				player.CargoHold.UpdateContainer();

				Port.gameManager.mlog.NewMessage("Sold " + Quantity.ToString() + " units of " + PortItem.Name + " for " + (Quantity * (Econ.MarketPrices[PortItem.Name] * Port.SellFactor)).ToString("F2"));

				InputField.text = 0.ToString();
				GameObject.Find("MarketMenu").GetComponent<MarketMenuController>().UpdateMarketMenu();
			}
		}


	}	
}
