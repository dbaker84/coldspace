﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIShip : Spacecraft {

	public GameObject Target;
	public Vector3 CoursePoint;
	public float ThrottlePos;

	public string Role; //trader, pirate, bounty hunter, etc.
	public bool EngageTarget;

	public float RecalcCourseTicker;
	public float RecalcCourseFreq;

	Rigidbody rb;
	StarController sun;

	//fighter variables
	public float RangeToTarget;
	public float IdealRange;
	public float FiringRange;

	public LayerMask CourseCollisionMask;
	public bool CollisionCourse;

	public PortController PortDestination;

	public List<GameObject> Threats = new List<GameObject>();
	public float ThreatDuration;

	public List<GameObject> Turrets = new List<GameObject>();

	void Start()
	{
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		rb = GetComponent<Rigidbody>();
		sun = GameObject.Find("Sun").GetComponent<StarController>();

		RecalcCourseTicker = RecalcCourseFreq;

		foreach (Transform child in transform)
		{
			if(child.tag == "Turret") Turrets.Add(child.gameObject);
		}
		//Target = GameObject.Find("Player");
	}

	void Update()
	{
		if(RecalcCourseTicker > 0) RecalcCourseTicker -= Time.deltaTime;

		if(Role == "Pirate")
		{
			if (Target) {
				RangeToTarget = (transform.position - Target.transform.position).magnitude;
				
				if(RangeToTarget >= IdealRange) ThrottlePos = 1.0f;
				else ThrottlePos = 0.1f;

				if(RangeToTarget <= FiringRange && EngageTarget) ActiveWeapon.AttemptToFire();

				if(Target.GetComponent<Spacecraft>())
				{
					if(Target.GetComponent<Spacecraft>().Landed) Target = null;
				}
			}
		}

		if(Role == "Trader")
		{
			if(PortDestination == null && !Landed)
			{
				PortDestination = sun.PortList[Random.Range(0, sun.PortList.Count)];
				//Debug.Log(Name + " is heading to " + PortDestination.PortName);
				ThrottlePos = 1.0f;
			}
		}




	}

	void FixedUpdate()
	{
		if(Role == "Pirate")
		{
			if (Target) {

				CoursePoint = Target.transform.position;
				Vector3 Forward = transform.TransformDirection(Vector3.forward).normalized;
				Vector3 ToTarget = (transform.position - CoursePoint).normalized;

				if(Vector3.Dot(Forward, ToTarget) < 0.99f)
				{
					float TurnRatio = Mathf.Clamp(1.02f - Vector3.Dot(Forward, ToTarget), 0, 1);
					if (Vector3.Angle(Forward, ToTarget) > 5) TurnRatio = 1.0f;
					else Mathf.Clamp(Vector3.Angle(Forward, ToTarget) / 180, 0, 1);

					Vector3 relativePoint = transform.InverseTransformPoint(CoursePoint);
					if (relativePoint.x > 0.0) TurnRatio *= -1;
						//print ("Object is to the left");
					//else if (relativePoint.x < 0.0)
						//print ("Object is to the right");

					rb.AddTorque(Vector3.up * TurnRatio * RotationSpeed);
				}

				rb.AddForce(-transform.forward * Thrust * ThrustModifier * ThrottlePos);
			}
		}

		if(Role == "Trader")
		{
			if(PortDestination != null && !Landed)
			{
			// 	if(RecalcCourseTicker <= 0)
			// 	{

			// 	}
				float TurnRatio = 0.5f;
				CoursePoint = PortDestination.transform.position;
				Vector3 Forward = transform.TransformDirection(Vector3.forward).normalized;
				Vector3 ToTarget = (transform.position - CoursePoint).normalized;
				RangeToTarget = (transform.position - PortDestination.transform.position).magnitude;

				RaycastHit hit;
				Debug.DrawLine(transform.position, transform.position + (-Forward *rb.velocity.magnitude * 10), Color.red, Time.deltaTime);

				if(RangeToTarget < 50 && !Physics.Linecast(transform.position, CoursePoint, CourseCollisionMask))
				{
					TurnRatio = Mathf.Clamp(Vector3.Angle(Forward, ToTarget) / 180, 0, 1);

					Vector3 relativePoint = transform.InverseTransformPoint(CoursePoint);
					if (relativePoint.x > 0.0) TurnRatio *= -1;
						//print ("Object is to the left");
					//else if (relativePoint.x < 0.0)
						//print ("Object is to the right");

					rb.AddTorque(Vector3.up * TurnRatio * RotationSpeed);

					Debug.Log(Name + " has a straight line into port");
				}
				else if (Physics.SphereCast(transform.position, 2.0f, -Forward, out hit, rb.velocity.magnitude * 10, CourseCollisionMask))
				{

					rb.AddTorque(Vector3.up * TurnRatio * RotationSpeed);
					//Debug.Log(Name + " is on a collision course.");

						//RecalcCourseTicker = RecalcCourseFreq;
						//CollisionCourse = true;
						//if(Random.Range(1,100) > 50) TurnRatio *= -1;

						
				}
				else if(Vector3.Dot(Forward, ToTarget) < 0.99f)
				{
					//TurnRatio = Mathf.Clamp(1.02f - Vector3.Dot(Forward, ToTarget), 0, 1);
					TurnRatio = Mathf.Clamp(Vector3.Angle(Forward, ToTarget) / 180, 0, 1);

					Vector3 relativePoint = transform.InverseTransformPoint(CoursePoint);
					if (relativePoint.x > 0.0) TurnRatio *= -1;
						//print ("Object is to the left");
					//else if (relativePoint.x < 0.0)
						//print ("Object is to the right");

					rb.AddTorque(Vector3.up * TurnRatio * RotationSpeed);
				}

				
				
				if(RangeToTarget >= IdealRange) ThrottlePos = 1.0f;
				else ThrottlePos = RangeToTarget / IdealRange;
				
				rb.AddForce(-transform.forward * Thrust * ThrustModifier * ThrottlePos);

				// if(RecalcCourseTicker <= 0 && CollisionCourse)
				// {
				// 	CollisionCourse = false;
				// }
			}
		}

		
	}

	void OnTriggerEnter (Collider c)
    {
        if(c.gameObject.tag == "Dock")
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            transform.parent = c.transform.parent;
            rb.isKinematic = true;
            ThrustModifier = 0;
            Landed = true;

			ThrottlePos = 0;
			
			if(c.gameObject.GetComponent<PortController>()) LandedPort = c.gameObject.GetComponent<PortController>();
			LandedPortGO = c.gameObject.transform.parent.gameObject;

			PortDestination = null;

			WeaponsHot = false;

			gameManager.mlog.NewMessage(Name + " has landed at " + LandedPort.PortName);
        }
    }
	
	public void LaunchFromPort()
	{
		float startTime = Time.time;
		Vector3 EjectVector = (transform.position - transform.parent.position).normalized;
		transform.parent = null;
		rb.isKinematic = false;
		ThrustModifier = 0.2f;
		Landed = false;
		LandedPort = null;

		//eject from pad
		

		rb.AddForce(EjectVector * 4.5f, ForceMode.Impulse);

		LandedPortGO = null;

		//gameManager.mlog.NewMessage("See you, Space Cowboy");
	}


	public void UpdateTurrets()
	{
		foreach(GameObject turret in Turrets)
		{
			if(turret.GetComponent<TurretController>().Target == null)
			{
				turret.GetComponent<TurretController>().Target = Threats[Random.Range(0, Threats.Count)];
			}
		}
	}

}
