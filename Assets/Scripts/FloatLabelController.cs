﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatLabelController : MonoBehaviour {

	public Transform MasterObject;
	public Vector3 Offset;
	public Camera MainCamera;


	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate () {
		transform.position = MainCamera.WorldToScreenPoint(MasterObject.position + Offset);
	}
}
