﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Suicider : MonoBehaviour {

	public float TimeTilSuicide;
	
	void Awake () {
		Destroy(gameObject, TimeTilSuicide);
	}

}
