﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Weapon : MonoBehaviour{

	public string Name;
	public string Mount; //"Fixed" or "Turret";
	public float RotationalLimit; // -1 can rotate 360 degrees, 0 can rotate 180 (facing forward), 1 cannot rotate
	public string Type; //"Beam" or "Projectile"
    public float DotLimit;

    public GameObject Target;

    private PlayerController player;
	LineRenderer lineRenderer;

    public bool sightLaser;
    public bool beamOnFire;
	//public GameObject particleSpray;

    public Camera MainCamera;
    
    public float damage;
    public float range;
    public float shotCost;
    public bool automatic;
    public float roundsPerSecond;
    private float repeatFireTicker;
    public LayerMask hitMask;

    public bool projectileType;
    public GameObject projectilePrefab;

    private AudioSource audioSource;
    public AudioClip FireSound;

    public float projectileVelocity;
    public float shotVariation; //degrees of random arc

    public bool beamType;


	void Start () {
	    //lineRenderer = gameObject.GetComponent<LineRenderer>();
        range = Mathf.Infinity;
        repeatFireTicker = 0;
        player = GameObject.Find("Player").GetComponent<PlayerController>();
        audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if(repeatFireTicker > 0)
            repeatFireTicker -= roundsPerSecond * Time.deltaTime;

    }

		

    public void ActivateEquipment()
    {
 
        Vector3 TargetVector = Vector3.zero;

        if(transform.parent.tag == "PlayerShip")
        {
            Vector3 MousePos = Input.mousePosition;
            MousePos.z = 10;
            MousePos = MainCamera.ScreenToWorldPoint(MousePos);
            MousePos.y = 0;
            //Debug.Log("Mouse at " + MousePos + " Player at " + transform.position);
            TargetVector = MousePos - transform.position;
        }
        else if(tag == "Turret")
        {
            TargetVector = GetComponent<TurretController>().Target.transform.position - transform.position;
        }
        else if(transform.parent.tag == "AIShip")
        {
            TargetVector = transform.parent.GetComponent<AIShip>().Target.transform.position - transform.position;
        }






        if(projectileType)
        {
            //if(beamOnFire) lineRenderer.enabled = true;
            //lineRenderer.SetPosition(1, hit.point - (Vector2)transform.position);

            Vector3 FinalAim = Quaternion.Euler(0, 1 * Random.Range(-shotVariation, shotVariation), 0) * TargetVector.normalized;
            float DotCheck = Vector3.Dot(FinalAim, -transform.forward);
            if (DotCheck >= DotLimit)
            {            
                GameObject proj = Instantiate(projectilePrefab,transform.position,transform.rotation);
                proj.GetComponent<ProjectileController>().FiredBy = transform.parent.gameObject;
                proj.GetComponent<ProjectileController>().Damage = damage;
                proj.transform.position = transform.position + (-proj.transform.forward * 0.5f);
                FinalAim = Quaternion.Euler(1 * Random.Range(-shotVariation, shotVariation), 1 * Random.Range(-shotVariation, shotVariation), 1 * Random.Range(-shotVariation, shotVariation)) * TargetVector.normalized;
                proj.transform.rotation = Quaternion.LookRotation(FinalAim);
                proj.GetComponent<Rigidbody>().AddForce(FinalAim * projectileVelocity, ForceMode.Impulse);

                audioSource.PlayOneShot(FireSound, 0.7F);
            }
            else
            {
                //FinalAim = Quaternion.Euler(0, 1 * Random.Range(-shotVariation, shotVariation), 0) * -transform.forward.normalized;
                //OUT OF ARC - SOUND?
            }

        }

    }

    public void AttemptToFire()
    {
        if(repeatFireTicker <= 0)
        {
            ActivateEquipment();
            repeatFireTicker = 1;
        }
    }

}
