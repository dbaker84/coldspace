﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketBuyButton : MonoBehaviour {


	public InvObj EntryItem;
	public PortController Port;
	public PlayerController player;

	public int Quantity;
	public Text InputField;

	public void Start()
	{
		GetComponent<Button>().onClick.AddListener(BuyItem);		
	}

	
	public void BuyItem()
	{
		Quantity = int.Parse(InputField.text);
		if(Quantity > 0)
		{			
			InvObj PlayerItem = player.CargoHold.Contents.Find(r => r.Name == EntryItem.Name);
			InvObj PortItem = Port.PortStorage.Contents.Find(r => r.Name == EntryItem.Name);

			if(Quantity <= PortItem.Units && player.Money >= Quantity * (Econ.MarketPrices[PortItem.Name] * Port.BuyFactor) && player.CargoHold.CapAvail >= Quantity)
			{
				//see if the player already has some of this item
				if(PlayerItem != null)
				{
					PlayerItem.Units += Quantity;
				}
				else
				{
					player.CargoHold.AddItem(new InvObj(PortItem.Name, Quantity));
				}

				PortItem.Units -= Quantity;	

				player.Money -= Quantity * (Port.GetPrice(PortItem.Name) * (1.0f + Econ.MarketArbitrage));
				Port.Money += Quantity * (Port.GetPrice(PortItem.Name) * (1.0f + Econ.MarketArbitrage));
			
				Port.gameManager.mlog.NewMessage("Bought " + Quantity.ToString() + " units of " + PortItem.Name + " for " + (Quantity * (Econ.MarketPrices[PortItem.Name] * Port.BuyFactor)).ToString("F2"));

				player.CargoHold.UpdateContainer();

				InputField.text = 0.ToString();
				GameObject.Find("MarketMenu").GetComponent<MarketMenuController>().UpdateMarketMenu();
			}	
			
		}


	}

}
