﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RumorManager : MonoBehaviour {

	private GameManager gameManager;

	// Use this for initialization
	void Start () {
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	public void GenerateRumor () {
		PortController RandomPort = GameObject.Find("Sun").GetComponent<StarController>().PortList[Random.Range(0,
			GameObject.Find("Sun").GetComponent<StarController>().PortList.Count)];
		float MaxIndex = 0.0f;
		string MaxItem = "";
		foreach(string Item in Econ.ItemList)
		{
			if(Mathf.Abs(RandomPort.PriceIndex[Item]) > MaxIndex)
			{
				MaxIndex = RandomPort.PriceIndex[Item];
				MaxItem = Item;
			}
		}
		if(MaxIndex > 0) gameManager.mlog.NewMessage("RUMOR: You hear that there is a " + MaxItem.ToLower() + " shortage at " +
			RandomPort.GetComponent<PortController>().PortName);
		else gameManager.mlog.NewMessage("RUMOR: You hear that there is a " + MaxItem.ToLower() + " surplus at " +
			RandomPort.GetComponent<PortController>().PortName);
	}
}
