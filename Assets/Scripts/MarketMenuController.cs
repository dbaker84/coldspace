﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketMenuController : MonoBehaviour {

	public GameObject MarketMenuEntryPrefab;
	public GameObject MarketEntryParent;
	
	public Vector3 StartPosition;
	public Vector3 Offset;

	public PlayerController player;
	public PortController port;

	public void UpdateMarketMenu ()
	{
		port = player.LandedPort;
		foreach(Transform go in MarketEntryParent.transform)
		{
			Destroy(go.gameObject);
		}

		transform.Find("MarketPortNameLabel").GetComponent<Text>().text = port.PortName;
		transform.Find("PlayerMoneyLabel").GetComponent<Text>().text = Mathf.Round(player.Money) + " Woolongs";
		transform.Find("PlayerCargoLabel").GetComponent<Text>().text = "Cargo Hold: " + Mathf.Round(player.CargoHold.CapUsed) + " / " + Mathf.Round(player.CargoHold.Capacity);
		transform.Find("PortDescLabel").GetComponent<Text>().text = port.GetPortDesc();


		float ticker = 0;

		foreach(InvObj entry in port.PortStorage.Contents)
		{
			//GameObject MenuEntry = Instantiate(MarketMenuEntryPrefab, StartPosition + (Offset * ticker), transform.rotation);
			GameObject MenuEntry = Instantiate(MarketMenuEntryPrefab, (Offset * ticker), transform.rotation);
			MenuEntry.transform.SetParent(MarketEntryParent.transform);
			MenuEntry.transform.localPosition = (Offset * ticker);

			MenuEntry.transform.Find("MarketEntryLabel").GetComponent<Text>().text = entry.Name;
			MenuEntry.transform.Find("MarketEntryPortQuant").GetComponent<Text>().text = entry.Units.ToString();

			InvObj PlayerCheck = player.CargoHold.Contents.Find(r => r.Name == entry.Name);
			if(PlayerCheck != null)
				MenuEntry.transform.Find("MarketEntryPlayerQuant").GetComponent<Text>().text = PlayerCheck.Units.ToString();
			else
				MenuEntry.transform.Find("MarketEntryPlayerQuant").GetComponent<Text>().text = "0";
				
			MenuEntry.transform.Find("MarketEntryBuyPrice").GetComponent<Text>().text = Mathf.Round(port.GetPrice(entry.Name) * (1.0f + Econ.MarketArbitrage)).ToString();
			MenuEntry.transform.Find("MarketEntrySellPrice").GetComponent<Text>().text = Mathf.Round(port.GetPrice(entry.Name) * (1.0f - Econ.MarketArbitrage)).ToString();

			

			MenuEntry.transform.Find("MarketEntrySellButton").GetComponent<MarketSellButton>().Port = port;
			MenuEntry.transform.Find("MarketEntrySellButton").GetComponent<MarketSellButton>().EntryItem = entry;
			MenuEntry.transform.Find("MarketEntrySellButton").GetComponent<MarketSellButton>().player = player;

			MenuEntry.transform.Find("MarketEntryBuyButton").GetComponent<MarketBuyButton>().Port = port;
			MenuEntry.transform.Find("MarketEntryBuyButton").GetComponent<MarketBuyButton>().EntryItem = entry;
			MenuEntry.transform.Find("MarketEntryBuyButton").GetComponent<MarketBuyButton>().player = player;
			//Debug.Log(port.name);
			ticker++;
		}
	}

	public void CloseMenu()
	{
		gameObject.SetActive(false);
		player.InMenu = false;
	}


}
