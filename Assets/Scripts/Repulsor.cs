﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repulsor : MonoBehaviour {

	public float RepulsePower;

	void OnTriggerEnter(Collider c)
    {
		if(c.gameObject.tag == "PlayerShip" || c.gameObject.tag == "AIShip")
		{
			Vector3 EjectVector = (c.gameObject.transform.position - transform.position).normalized;
			c.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;
			c.gameObject.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			c.gameObject.GetComponent<Rigidbody>().AddForce(EjectVector * RepulsePower, ForceMode.Impulse);
			c.gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.up * (Random.Range(-1,1)));

			// Vector3 EjectVector = ((c.gameObject.GetComponent<Rigidbody>().velocity +
			// GetComponent<Spacecraft>().SizeClass * GetComponent<Rigidbody>().velocity)) / (1 + GetComponent<Spacecraft>().SizeClass);
			// c.gameObject.GetComponent<Rigidbody>().AddForce(EjectVector, ForceMode.Impulse);
			// //c.gameObject.GetComponent<Rigidbody>().AddTorque(Vector3.up * 1000 * (Random.Range(0,2)*2-1));
		}

	}

}
