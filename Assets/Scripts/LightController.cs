﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

	public GameObject Target;

	void Update () {
		transform.LookAt(Target.transform);
		Vector3 v = transform.rotation.eulerAngles;
        transform.rotation = Quaternion.Euler (0, v.y, 0);
	}
}
