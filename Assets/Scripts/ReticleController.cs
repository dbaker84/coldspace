﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleController : MonoBehaviour {
	public Camera MainCamera;
	public PlayerController player;
	public Color InArcColor;
	public Color OutArcColor;

	void Update()
	{
		
		if(MainCamera.isActiveAndEnabled)
		{
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10;
			Vector3 point = Camera.main.ScreenToWorldPoint(mousePos);
			transform.position = point;
			//Debug.Log(mousePos + " " + point);

			float DotCheck = -(Vector3.Dot((player.transform.position - new Vector3(point.x, 0, point.z)).normalized, -player.transform.forward));
			//Debug.Log((player.transform.position - new Vector3(point.x, 0, point.z)));
			//Debug.Log(DotCheck);
			if (DotCheck < player.ActiveWeapon.DotLimit) GetComponent<Renderer>().material.color = OutArcColor;
			else GetComponent<Renderer>().material.color = InArcColor;
		}
			

	}

}
