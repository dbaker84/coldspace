﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : MonoBehaviour {

	public GameObject FiredBy;
	public float Damage;
	public float ArmorPiercingRating; // 0 to 1    1 bypasses all armor
	public GameObject ImpactSpray;
	public float Lifetime;

	void Start()
	{
		Destroy(gameObject, Lifetime);
	}

	void OnTriggerEnter(Collider c)
	{
		if(c.gameObject != FiredBy && c.tag != "Projectile")
		{

			RaycastHit hit;
			Vector3 POI = transform.position;
			if (Physics.Raycast(transform.position, transform.forward, out hit))
			{
				POI = hit.point;
			}
			GameObject p = Instantiate(ImpactSpray, POI, transform.rotation);
			Destroy(p, p.GetComponent<ParticleSystem>().main.duration);
			Destroy(gameObject);

			if(c.gameObject.GetComponent<Spacecraft>())
			{
				c.gameObject.GetComponent<Spacecraft>().ApplyDamage(Damage, ArmorPiercingRating);
				if(c.gameObject.GetComponent<AIShip>())
				{
					c.gameObject.GetComponent<AIShip>().Threats.Add(FiredBy);
					c.gameObject.GetComponent<AIShip>().UpdateTurrets();
				}
			}

		}
	}

}
